from pymongo import MongoClient

client = MongoClient('143.110.191.155', 37217)
db = client["PoojaChowdary"]
# Collection = db["user_details"]
mycol = db["user_details"]


class Commonutility:
    @staticmethod
    def insert(records):
        if isinstance(records, list):
            mycol.insert_many(records)
        else:
            mycol.insert_one(records)

    @staticmethod
    def findonem():
        findone = mycol.find_one()
        print(findone)
        return findone
        # v=db.demo230.findOne({"first_name":"Kirby"})
        # print(v)

    @staticmethod
    def findall():
        data = []
        for record in mycol.find():
            data.append(record)
        return data

    @staticmethod
    def update():
        myquery = {"first_name": "Ingra", "last_name": "Hefferon"}
        newvalues = {"$set": {"last_name": "mulaguri"}}
        mycol.update_one(myquery, newvalues)
        for record in mycol.find({"first_name": "Ingra", "last_name": "mulaguri"}):
            return record

    @staticmethod
    def delete():
        list1 = []
        result = mycol.delete_many({"gender": "Female"})
        k = "no of records deleted" + str(result.deleted_count) + "     "
        for record in mycol.find():
            list1.append(record)
        return k, list1

    @staticmethod
    def drop():
        list1 = []
        db.drop_collection(mycol)
        for record in mycol.find():
            list1.append(record)
        return list1
