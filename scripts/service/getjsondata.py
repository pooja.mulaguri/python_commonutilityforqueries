from flask import request, Blueprint
from scripts.handler.commonutility import Commonutility

blueprint = Blueprint("blueprint", __name__)


@blueprint.route('/', methods=['POST'])
def postjsonhand():
    content = request.get_json()
    Commonutility.insert(content)

    return str(content)


@blueprint.route('/find/', methods=['GET'])
def find():
    k = Commonutility.findonem()
    return str(k)


@blueprint.route('/findall')
def findalldata():
    k = Commonutility.findall()
    return str(k)


@blueprint.route('/update')
def updatedata():
    k = Commonutility.update()
    return str(k)


@blueprint.route('/delete')
def deletedata():
    k = Commonutility.delete()
    return str(k)


@blueprint.route('/drop')
def dropdata():
    k = Commonutility.drop()
    return str(k)
